#!/usr/bin/perl
# by V.Varbanovski @nu11secur1ty
use strict;
use warnings;
use diagnostics;

print "Type domain to check for IP\n";
print "Or if you already know the IP address, just press ENTER\n";

	our $IP_checker = <STDIN>;
	our $checker = `host $IP_checker`;
		print "The IP of your domain $IP_checker is\n $checker\n";
print "Type your IP\n";
	our $domain_ip = <>;
	our $scare = `ab -n 100 -c 10 "http://$domain_ip/"`;
		print "The Apache-Bench test of your domain $IP_checker is finished\n\n\n";
			print "-----------------------------------------------------------\n";
		print "The result is: $scare\n";
exit 0;

